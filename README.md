# Taller 1 - Particle Physics

## Description

Data analysis project from an ATLAS Opendata example of a Higgs analysis (4-lepton final state). Students should repeat the analysis and improve the signal-background separation by creating/implementing new selections.

## MC Files

- [ ] [ATLAS Opendata](https://drive.google.com/drive/folders/1kKnHR8gqhdZjHM01FVGQZE5bUbeHQoLY?usp=drive_link)

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

- Sebastian Olivares (sebastian.olivares@unab.cl)
